import React from 'react';
import './App.css';
import Amplify from 'aws-amplify';
import { AmplifyAuthenticator, AmplifySignOut, AmplifySignUp } from '@aws-amplify/ui-react';
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components';
import axios from 'axios';
import ReactJson from 'react-json-view'
import config from './configs'


Amplify.configure({
    Auth: {
        region: 'us-east-2',
        userPoolId: 'us-east-2_UflFqAUo1',
        userPoolWebClientId: '15fcfvl084dcvpljbcbkrvc094',
    },
});

const AuthStateApp = () => {
    const [authState, setAuthState] = React.useState();
    const [user, setUser] = React.useState();
    const [statusSuccess, setStatusSuccess] = React.useState();
    const [statusError, setStatusError] = React.useState();
    const [data, setData] = React.useState();

    React.useEffect(() => {
        return onAuthUIStateChange((nextAuthState, authData) => {
            setAuthState(nextAuthState);
            setUser(authData);

            if (nextAuthState === AuthState.SignedIn && authData) {
                const jwtToken = authData.signInUserSession.idToken.jwtToken;
                axios.post(config.HOST_URL, {}, {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + jwtToken,
                    },
                }).then(response => {
                    setData(response.data);
                    setStatusSuccess(response.status);
                }).catch(error => {
                    if (error.response) {
                        setData(error.response.data);
                        setStatusError(error.response.status);
                    }
                })
            }

        });
    }, []);

    return (
        <div>
            {authState === AuthState.SignedIn && user ? (
                <div class="container h-100">
                    <div class="mt-md-5">
                        {statusSuccess === 200 ? (
                            <button type="button" class="btn btn-success">
                                Status code: <span class="badge badge-light">{statusSuccess}</span>
                            </button>
                        ) : (
                                <button type="button" class="btn btn-danger">
                                    Status code: <span class="badge badge-light">{statusError}</span>
                                </button>
                            )}
                    </div>
                    <div class="mt-md-5">
                        <div class="card">
                            <div class="card-header">
                                Content of Response
                            </div>
                            <div class="card-body">
                                <ReactJson src={data} />
                            </div>
                        </div>
                    </div>
                    <div>
                        <AmplifySignOut />
                    </div>
                </div>
            ) : (
                    <AmplifyAuthenticator>
                        <AmplifySignUp
                            slot="sign-up"
                            formFields={[
                                { type: "username" },
                                { type: "password" },
                            ]}
                        />
                    </AmplifyAuthenticator>)
            }
        </div>
    )
};

export default AuthStateApp;
